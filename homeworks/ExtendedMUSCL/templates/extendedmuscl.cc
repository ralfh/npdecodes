/**
 * @file extendedmuscl.cc
 * @brief NPDE homework ExtendedMUSCL code
 * @author Oliver Rietmann
 * @date 04.08.2019
 * @copyright Developed at ETH Zurich
 */

#include "extendedmuscl.h"

#include <algorithm>
#include <cmath>
#include <initializer_list>

namespace extendedmuscl {

double logGodunovFlux(double v, double w) {
  double godunov_numerical_flux;
  auto f = [](double u) { return u * (std::log(u) - 1.0); };
  auto df = [](double u) { return std::log(u); };

  /* SOLUTION_BEGIN */
  

  //                         WRITE YOUR SOLUTION HERE
                         

  /* SOLUTION_END */

  return godunov_numerical_flux;
}

double limiterMC(double mu_left, double mu_center, double mu_right) {
  double scaled_slope;

  /* SOLUTION_BEGIN */
  

  //                         WRITE YOUR SOLUTION HERE
                         

  /* SOLUTION_END */

  return scaled_slope;
}

} // namespace extendedmuscl
