#include <iostream>
#include <memory>

#include <lf/assemble/assemble.h>
#include <lf/base/base.h>
#include <lf/geometry/geometry.h>
#include <lf/mesh/hybrid2d/hybrid2d.h>
#include <lf/mesh/utils/utils.h>
#include <lf/uscalfe/uscalfe.h>

#include "gradprojection.h"

int main() {
  // for this exercise a main file is not required
  // but feel free to use it to call some of your functions for debugging
  // purposes
  // BEGIN_SOLUTION
  // TODO Your implementation goes here!
  // END_SOLUTION

  std::cout << "You may use this main file to call your function "
               "ProjectionOntoGradients::ProjectionOntoGradients"
            << std::endl;
}
