/** @file
 * @brief NPDE SDIRKMethodOfLines
 * @author Erick Schulz
 * @date 12/04/2019
 * @copyright Developed at ETH Zurich
 */

#include "sdirkmethodoflines_ode.h"

namespace SDIRKMethodOfLines {

/* SAM_LISTING_BEGIN_1 */
std::vector<double> sdirk2SteppingLinScalODE(unsigned int m) {
  std::vector<double> sol_vec;
  /* SOLUTION_BEGIN */
    // Write your code here ...
  /* SOLUTION_END */
  return sol_vec;
}
/* SAM_LISTING_END_1 */

void sdirk2ScalarODECvTest() {

  /* SOLUTION_BEGIN */
   // Write your code here ...
  /* SOLUTION_END */
}

}  // namespace SDIRKMethodOfLines
